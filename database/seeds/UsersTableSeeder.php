<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
          'nickname' => "Admin",
          'email' => "admin@moviepocket.local",
          'password' => bcrypt("Br0k3nL!f3"),
          'type' => App\User::TYPE_Owner, // Owner
          'name' => "Administrator",
          'birthdate' => "1970-01-01"
        ]);
        $this->command->info("\nCreated administartor account.");
        $this->command->comment("Email:\tadmin@moviepocket.local\nPass:\tBr0k3nL!f3");
    }
}
