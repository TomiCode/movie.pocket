<li class="media mx-4 my-2">
  <div class="media-body">
    <h6 class="m-0 d-inline-block">
      <a class="text-secondary" href="{{ route('users.show', $comment->user) }}">
        {{ $comment->user->name }} {{ $comment->user->surname }}
      </a>
    </h6>
    @auth
      @if($comment->user == auth()->user() || auth()->user()->isModerator())
        <small>
          <a href="{{ route('comments.delete', $comment) }}">
            <i class="fas fa-trash ml-2 text-danger"></i>
          </a>
        </small>
      @endif
    @endauth
    <div class="small">{{ $comment->content }}</div>
  </div>
</li>