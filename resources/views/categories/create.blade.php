@extends('layouts.app')

@section('content')
<div class="container">
  <div class="row justify-content-center">
    <div class="col-md-8">
      <div class="card">
        <div class="card-header">{{ __('Dodawanie kategorii') }}</div>
        <div class="card-body">
          <form method="POST" action="{{ route('categories.store') }}" >
            @csrf
            <div class="form-group row">
              <label class="col-sm-4 col-form-label">Nazwa</label>
              <div class="col-sm-8">
                <input type="text" name="name" placeholder="nowa kategoria" value="{{ old('name') }}" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}">
                @if ($errors->has('name'))
                  <div class="invalid-feedback">
                    <strong>{{ $errors->first('name') }}</strong>
                  </div>
                @endif
              </div>
            </div>
            <div class="form-group row mb-0">
              <div class="col-md-8 offset-md-4">
                <button type="submit" class="btn btn-primary">
                  {{ __('Utwórz') }}
                </button>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection