@extends('layouts.app')

@section('content')
<div class="container mb-5">
  <h2>{{ $category->name }} <small>&mdash; filmy kategorii</small><hr></h2>
  <div class="card-columns m-2">
    @each('films.single', $category->films, 'film', 'films.empty')
  </div>
</div>
@endsection