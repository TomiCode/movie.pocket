<a class="card bg-secondary text-white" href="{{ route('categories.show', [$category]) }}">
  <div class="card-header">{{ $category->name }}</div>
</a>