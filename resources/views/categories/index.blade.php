@extends('layouts.app')

@section('content')

<div class="container">
  <h2>Kategorie filmów <hr></h2>
  <div class="card-columns">
    @each('categories.single', $categories, 'category')
  </div>
</div>

@endsection