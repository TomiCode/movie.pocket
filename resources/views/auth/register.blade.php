@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Rejestracja użytkownika') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('register') }}">
                        @csrf

                        <div class="form-group">
                            <div class="alert alert-secondary">
                                Zakładając w systemie konto, akteptujesz warunki przechowywania danych w systemie.
                            </div>
                        </div>

                        <div class="form-group">
                            <label>{{ __('Nazwa użytkownika') }}</label>
                            <input id="nickname" type="text" name="nickname" value="{{ old('nickname') }}" class="form-control{{ $errors->has('nickname') ? ' is-invalid' : '' }}" required>
                            @if ($errors->has('nickname'))
                                <div class="invalid-feedback">
                                    <strong>{{ $errors->first('nickname') }}</strong>
                                </div>
                            @endif
                        </div>
                        <div class="form-group">
                            <label>{{ __('Adres e-mail') }}</label>
                            <input id="email" type="email" class="form-control {{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>
                            @if ($errors->has('email'))
                                <div class="invalid-feedback">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </div>
                            @endif
                        </div>
                        <div class="form-group">
                            <label>{{ __('Imię') }}</label>
                            <input id="name" type="text" class="form-control {{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name') }}" required>
                            @if ($errors->has('name'))
                                <div class="invalid-feedback">
                                    <strong>{{ $errors->first('name') }}</strong>
                                </div>
                            @endif
                        </div>
                        <div class="form-group">
                            <label>{{ __('Nazwisko') }}</label>
                            <input id="surname" type="text" class="form-control {{ $errors->has('surname') ? ' is-invalid' : '' }}" name="surname" value="{{ old('surname') }}">
                            @if ($errors->has('surname'))
                                <div class="invalid-feedback">
                                    <strong>{{ $errors->first('surname') }}</strong>
                                </div>
                            @endif
                        </div>
                        <div class="form-group">
                            <label>{{ __('Data urodzenia') }}</label>
                            <div class="input-group {{ $errors->has('birthdate') ? 'is-invalid' : '' }}">
                                <select name="birthdate_day" class="custom-select">
                                    @for ($i = 1; $i <= 31; $i++)
                                        <option {{ old('birthdate_day') == $i ? 'selected' : '' }} value="{{ $i }}">{{ $i }}</option>
                                    @endfor
                                </select>
                                <select name="birthdate_month" class="custom-select">
                                    @foreach(['stycznia', 'lutego', 'marca', 'kwietnia', 'maja', 'czerwca', 'lipca', 'sierpnia', 'września', 'października', 'listopada', 'grudnia'] as $month)
                                        <option {{ old('birthdate_month') == ($loop->index + 1) ? 'selected' : '' }} value="{{ $loop->index + 1 }}">{{ $month }}</option>
                                    @endforeach
                                </select>
                                <select name="birthdate_year" class="custom-select">
                                    @for ($i = 2014; $i >= 1951; $i--)
                                        <option {{ old('birthdate_year') == $i ? 'selected' : '' }} value="{{ $i }}">{{ $i }}</option>
                                    @endfor
                                </select>
                            </div>
                            @if ($errors->has('birthdate'))
                                <div class="invalid-feedback">
                                    <strong>{{ $errors->first('birthdate') }}</strong>
                                </div>
                            @endif
                        </div>
                        <div class="form-group">
                            <label>{{ __('Hasło') }}</label>
                            <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>
                            @if ($errors->has('password'))
                                <span class="invalid-feedback">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="form-group">
                            <label for="password-confirm">{{ __('Potwierdzenie hasła') }}</label>
                            <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Utwórz konto') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
