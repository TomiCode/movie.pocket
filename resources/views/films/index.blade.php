@extends('layouts.app')

@section('content')
<div class="container mb-5">
  <h2>Baza filmów<hr></h2>
  <div class="card-columns m-2">
    @each('films.single', $films, 'film', 'films.empty')
  </div>
</div>
@endsection