<a href="{{ route('films.show', [$film]) }}" class="card text-secondary">
  <img class="card-img-top" src="/{{ $film->imgcover }}" alt="{{ $film->title }}">
  <div class="card-body">
    <h4 class="card-title">{{ $film->title }}</h4>
    <h6 class="card-subtitle">{{ $film->category->name }}</h6>
  </div>
  <div class="card-footer text-muted">
    <small>
    <i class="far fa-calendar mr-1"></i> {{ $film->release_date }}
      <div class="float-right">
        <i class="far fa-edit mr-1"></i> {{ $film->reviews->count() }}
      </div>
    </small>
  </div>
</a>