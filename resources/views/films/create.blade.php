@extends('layouts.app')

@section('content')
<div class="container">
  <div class="row justify-content-center">
    <div class="col-md-8">
      <div class="card">
        <div class="card-header">{{ __('Tworzenie filmu') }}</div>

        <div class="card-body">
          <form method="POST" action="{{ route('films.store') }}" enctype="multipart/form-data">
            @csrf
            <div class="form-group row">
              <label class="col-sm-4 col-form-label">Tytuł</label>
              <div class="col-sm-8">
                <input type="text" name="title" placeholder="tytuł" value="{{ old('title') }}" class="form-control{{ $errors->has('title') ? ' is-invalid' : '' }}">
                @if ($errors->has('title'))
                  <div class="invalid-feedback">
                    <strong>{{ $errors->first('title') }}</strong>
                  </div>
                @endif
              </div>
            </div>
            <div class="form-group row">
              <label class="col-sm-4 col-form-label">Premiera</label>
              <div class="col-sm-8">
                <input type="text" name="released" placeholder="rrrr-mm-dd" value="{{ old('released') }}" class="form-control{{ $errors->has('released') ? ' is-invalid' : '' }}">
                @if ($errors->has('released'))
                  <div class="invalid-feedback">
                    <strong>{{ $errors->first('released') }}</strong>
                  </div>
                @endif
              </div>
            </div>
            <div class="form-group row">
              <label class="col-sm-4 col-form-label">Gatunek</label>
              <div class="col-sm-8">
                <input type="text" name="genre" placeholder="gatunek" value="{{ old('genre') }}" class="form-control{{ $errors->has('genre') ? ' is-invalid' : '' }}">
                @if ($errors->has('genre'))
                  <div class="invalid-feedback">
                    <strong>{{ $errors->first('genre') }}</strong>
                  </div>
                @endif
              </div>
            </div>
            <div class="form-group row">
              <label class="col-sm-4 col-form-label">Reżyseria</label>
              <div class="col-sm-8">
                <input type="text" name="director" placeholder="reżyseria" value="{{ old('director') }}" class="form-control{{ $errors->has('director') ? ' is-invalid' : '' }}">
                @if ($errors->has('director'))
                  <div class="invalid-feedback">
                    <strong>{{ $errors->first('director') }}</strong>
                  </div>
                @endif
              </div>
            </div>
            <div class="form-group row">
              <label class="col-sm-4 col-form-label">Kategoria</label>
              <div class="col-sm-8">
                <select name="category" class="custom-select{{ $errors->has('category') ? ' is-invalid' : '' }}">
                  <option disabled="disabled"{{ old('category', null) ? '' : ' selected' }}>Wybierz kategorię..</option>
                  @foreach(\App\Category::all() as $category)
                    <option{{ old('category') == $category->id ? ' selected' : '' }} value="{{ $category->id }}">{{ $category->name }}</option>
                  @endforeach
                </select>
                @if ($errors->has('category'))
                  <div class="invalid-feedback">
                    <strong>{{ $errors->first('category') }}</strong>
                  </div>
                @endif
              </div>
            </div>
            <div class="form-group row">
              <label class="col-sm-4 col-form-label">Okładka</label>
              <div class="col-sm-8">
                <div class="custom-file">
                  <input type="file" name="cover" class="custom-file-input{{ $errors->has('cover') ? ' is-invalid' : '' }}">
                  <label class="custom-file-label">Wybierz grafikę..</label>
                  @if ($errors->has('cover'))
                    <div class="invalid-feedback">
                      <strong>{{ $errors->first('cover') }}</strong>
                    </div>
                  @endif
                </div>
              </div>
            </div>

            <div class="form-group row mb-0">
              <div class="col-md-8 offset-md-4">
                <button type="submit" class="btn btn-primary">
                  {{ __('Utwórz film') }}
                </button>
              </div>
            </div>

          </form>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection