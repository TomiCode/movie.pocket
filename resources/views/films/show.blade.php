@extends('layouts.app')

@section('content')
<div class="container">
  <h3 class="my-4">
    O filmie<hr class="m-0">
  </h3>
  <div class="row">
    <div class="col col-sm-auto">
      <img src="/{{ $film->imgcover }}" class="movie-cover rounded" alt="Movie cover">
    </div>
    <div class="col">
      <ul class="list-group list-group-flush">
        <li class="list-group-item">
          <h4 class="d-inline mr-1">{{ $film->title }}</h4>
          <a href="{{ route('categories.show', $film->category) }}" class="badge text-muted">{{ $film->category->name }}</a>
        </li>
        <li class="list-group-item">
          <i class="fas fa-podcast text-muted mr-2"></i> {{ $film->director }}
        </li>
        <li class="list-group-item">
          <i class="fas fa-film text-muted mr-2"></i> {{ $film->genre }}
        </li>
        <li class="list-group-item">
          <i class="far fa-calendar text-muted mr-2"></i> {{ $film->release_date }}
        </li>
      </ul>
    </div>
  </div>

  <h5 class="mt-5">
    <i class="far fa-edit mr-2 text-muted"></i> Recenzje
    <small>
      @auth
        @if($film->review)
          <a class="float-right text-info m-1" href="{{ route('reviews.show', $film->review) }}">
            <i class="far fa-arrow-right mr-2"></i> Moja recenzja
          </a>
        @else
          <a class="float-right text-secondary m-1" href="{{ route('reviews.create', $film) }}">
            <i class="far fa-plus mr-2"></i> Zrecenzuj..
          </a>
        @endif
      @endauth
    </small>
    <hr class="my-1">
  </h5>
  <ul class="list-unstyled mb-4">
    @each('reviews.single', $film->reviews, 'review', 'reviews.empty')
  </ul>
</div>
@endsection