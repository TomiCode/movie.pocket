<li class="media m-4">
  <div class="media-body">
    <h5 class="m-0">
      <a class="text-secondary" href="{{ route('users.show', $review->user) }}">
        {{ $review->user->name }} {{ $review->user->surname }}
      </a>
    </h5>
    <div class="d-inline">{{ str_limit($review->content) }}</div>
    <div class="media-footer">
      <div class="float-right">
        <a class="text-info" href="{{ route('reviews.show', $review) }}">więcej..</a>
      </div>
      <div class="text-muted d-inline-block mr-3">
        @for ($star = 1; $star <= 5; $star++)
          @if ($star <= $review->rating)
            <i class="fas fa-star text-info"></i>
          @else
            <i class="far fa-star"></i>
          @endif
        @endfor
      </div>
      <div class="text-muted d-inline-block">
        <i class="far fa-comments mr-1"></i> {{ $review->comments->count() }}</div>
      </div>
  </div>
</li>