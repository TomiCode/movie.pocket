@extends('layouts.app')

@section('content')
<div class="container">
  <div class="row justify-content-center">
    <div class="col-md-8">
      <div class="card">
        <div class="card-header">
          <a class="mr-3" href="{{ route('films.show', $film) }}"><i class="fas fa-chevron-left"></i></a>
          Tworzenie recenzji
        </div>

        <div class="card-body">
          <form method="POST" action="{{ route('reviews.store', $film) }}">
            @csrf
            <div class="form-group row">
              <label class="col-sm-4 col-form-label">Ocena</label>
              <div class="col-sm-8">
                <select name="rating" class="custom-select{{ $errors->has('rating') ? ' is-invalid' : '' }}">
                  <option disabled="disabled"{{ old('rating', null) ? '' : ' selected' }}>Oceń film..</option>
                  @for($note = 1; $note <= 5; $note++)
                    <option{{ old('rating') == $note ? ' selected' : '' }} value="{{ $note }}">{{ $note }}</option>
                  @endfor
                </select>
                @if ($errors->has('rating'))
                  <div class="invalid-feedback">
                    <strong>{{ $errors->first('rating') }}</strong>
                  </div>
                @endif
              </div>
            </div>
            <div class="form-group">
              <textarea placeholder="Treść recenzji.." name="content" rows="4" class="form-control{{ $errors->has('content') ? ' is-invalid' : '' }}">{{ old('content') }}</textarea>
              @if ($errors->has('content'))
                <div class="invalid-feedback">
                  <strong>{{ $errors->first('content') }}</strong>
                </div>
              @endif
            </div>

            <div class="form-group row mb-0">
              <div class="col-md-8 offset-md-4">
                <button type="submit" class="btn btn-info">Dodaj recenzję</button>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection