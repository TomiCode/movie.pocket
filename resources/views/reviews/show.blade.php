@extends('layouts.app')

@section('content')
<div class="container">
  <h4 class="mb-4">
    Szczegóły recenzji
    <hr class="my-1">
  </h4>
  <div class="row">
    <div class="col-auto px-0">
      <img class="ml-4 review-cover rounded" src="/{{ $review->film->imgcover }}" alt="Movie cover">
    </div>
    <div class="col">
      <ul class="list-group list-group-flush">
        <li class="list-group-item px-0">
          <h5 class="display-5 d-inline-block">{{ $review->film->title }}</h5>
          <a class="badge text-muted" href="{{ route('categories.show', $review->film->category) }}">{{ $review->film->category->name }}</a>
        </li>
        <li class="list-group-item px-0">
          <h6 class="text-secondary">
            <i class="far fa-user mr-1"></i>
            {{ $review->user->name }} {{ $review->user->surname }}
          </h6>
        </li>
      </ul>
    </div>
  </div>
  <div class="text-justify p-3 d-block">{{ $review->content }}</div>
  <h5 class="pt-2 my-3">
    <i class="far fa-comments mr-2 text-muted"></i> Komentarze
    <hr class="my-1">
  </h5>
  <ul class="list-unstyled mb-4">
    @each('comments.single', $review->comments, 'comment', 'comments.empty')
  </ul>

  @auth
    <div class="card my-5">
      <div class="card-body">
        <form method="POST" action="{{ route('comments.store', $review) }}">
          @csrf
          <div class="form-group">
            <textarea placeholder="Nowy komentarz.." name="content" rows="3" class="form-control{{ $errors->has('content') ? ' is-invalid' : '' }}"></textarea>
            @if ($errors->has('content'))
              <div class="invalid-feedback">
                <strong>{{ $errors->first('content') }}</strong>
              </div>
            @endif
          </div>
          <button type="submit" class="btn btn-info float-right">Dodaj komentarz</button>
        </form>
      </div>
    </div>
  @endauth
</div>
@endsection