@extends('layouts.app')

@section('content')
<div class="jumbotron jumbotron-fluid pb-0">
  <div class="container mb-5">
    <h2 class="display-4">MoviePocket</h2>
    <p class="lead">System recenzji filmów</p>
  </div>
  <div class="btn-group d-flex">
    <a href="{{ route('films.index') }}" class="btn btn-secondary w-100 rounded-0">Filmy</a>
    <a href="{{ route('categories.index') }}" class="btn btn-secondary w-100 rounded-0">Kategorie</a>
    <a href="{{ route('ranks.index') }}" class="btn btn-secondary w-100 rounded-0">Rankingi</a>
  </div>
</div>
<div class="container my-5">
  <h4 class="my-4">
    <i class="far fa-bell text-muted mr-2"></i>
    Nowości
    <hr class="my-1">
  </h4>
  <div class="card-deck m-2">
    @each('films.single', $films, 'film')
  </div>
</div>
@endsection
