@extends('layouts.app')

@section('content')
<div class="container">
  <h4 class="mb-4">
    @auth
      @if(auth()->user() == $user)
        <small class="float-right mt-1 ">
          <a class="text-info" href="{{ route('users.edit', $user) }}"><i class="fas fa-cog"></i> edytuj</a>
        </small>
      @endif
    @endauth
    {{ $user->name }} {{ $user->surname }}
    <hr class="my-1">
  </h4>
  <div class="row">
    <div class="col-sm-4 col-md-auto px-1">
      <img class="ml-sm-4 rounded d-block" src="{{ $user->gravatar() }}" alt="gravatar" style="margin: auto;">
    </div>
    <div class="col">
      <ul class="list-group list-group-flush">
        <li class="list-group-item px-0">
          <i class="far fa-envelope text-muted mr-2"></i>{{ $user->email }}
        </li>
        <li class="list-group-item px-0">
          <i class="far fa-birthday-cake text-muted mr-2"></i>{{ $user->birthdate->formatLocalized('%d %B %Y') }}
        </li>
      </ul>
    </div>
  </div>

  <h5 class="pt-5">
    <i class="far fa-edit text-muted mr-2"></i>
    Najnowsze recenzje<hr class="my-1">
  </h5>
  <ul class="list-unstyled mb-4">
    @each('reviews.single', $user->reviews->take(5), 'review', 'reviews.empty')
  </ul>
</div>
@endsection