@extends('layouts.app')

@section('content')
<div class="container">
  <h2 class="text-center">Rankingi</h2>
  <ul class="list-unstyled mb-4">
    @each('ranks.film', $films, 'film', 'films.empty')
  </ul>
</div>
@endsection