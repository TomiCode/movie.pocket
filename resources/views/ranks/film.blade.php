<h5 class="my-1">
  {{ (\Carbon\Carbon::parse($film->release_date))->year }}
  <hr class="my-1 mb-3">
</h5>
<div class="media mb-4">
  <img class="review-cover rounded mx-4" src="/{{ $film->imgcover }}" alt="film cover">
  <div class="media-body">
    <h5 class="m-0">
      <a class="text-secondary" href="{{ route('films.show', $film->id) }}">{{ $film->title }}</a>
    </h5>
    <div class="my-1"><i class="fas fa-podcast text-muted mr-2"></i>{{ $film->director }}</div>
    <div class="small mt-3 d-block">
      <div class="d-inline-block mr-2 text-secondary">
        @for ($star = 1; $star <= 5; $star++)
          @if ($star <= $film->rating)
            <i class="fas fa-star text-info"></i>
          @else
            <i class="far fa-star"></i>
          @endif
        @endfor
      </div>
      {{ number_format($film->rating, 2) }}
    </div>
  </div>
</div>