<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Comment;
use App\Review;

class CommentsController extends Controller
{
  public function __construct()
  {
    $this->middleware('auth');
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create(Review $review)
  {
    return view('comments.create')->with('review', $review);
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request, Review $review)
  {
    $request->validate([
      'content' => "required|string|min:3"
    ]);

    $comment = new Comment();
    $comment->content = $request->content;
    $comment->user()->associate(auth()->user());
    $review->comments()->save($comment);

    session()->flash('success', "Twój komentarz został dodany do systemu.");
    return redirect()->route('reviews.show', $review);
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function edit(Comment $comment)
  {
    return view('comments.edit')->with('comment', $comment);
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, Comment $comment)
  {
    $request->validate([
      'content' => 'required|string|min:3'
    ]);

    $comment->content = $request->content;
    $comment->save();

    session()->flash('success', "Komentarz został zaktualizowany.");
    return redirect()->route('reviews.show', $comment->review);
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function destroy(Comment $comment)
  {
    if (auth()->user() == $comment->user || auth()->user()->isModerator()) {
      $comment->delete();
      session()->flash('success', 'Komentarz został usunięty.');
    }
    else {
      session()->flash('error', 'Brak uprawnień do wykonania tej operacji.');
    }
    return redirect()->route('reviews.show', $comment->review);
  }
}
