<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

class UserController extends Controller
{
  public function show(User $user)
  {
    return view('users.show')->with('user', $user);
  }

  public function edit(User $user)
  {
    return view('users.edit')->with('user', $user);
  }

  public function update(Request $request, User $user)
  {
    if (auth()->user() != $user) {
      session()->flash('error', "Nie masz uprawnień aby edytować tego użytkownika.");
      return redirect()->route('home');
    }

    $request->request->add([
      'birthdate' => sprintf("%s-%s-%s", $request->birthdate_year, $request->birthdate_month, $request->birthdate_day)
    ]);

    $request->validate([
      'name' => 'required|string|max:255',
      'surname' => 'nullable|string|max:255',
      'birthdate' => 'required|date|before:-16 years'
    ]);
    $user->name = $request->name;
    $user->surname = $request->surname;
    $user->birthdate = $request->birthdate;
    $user->save();

    session()->flash('success', "Profil użytkownika został zaktualizowany!");
    return redirect()->route('users.show', $user);
  }
}
