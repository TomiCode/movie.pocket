<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class RankController extends Controller
{
  public function index()
  {
    $films = DB::select('SELECT * FROM (SELECT avg(reviews.rating) as rating, films.* FROM reviews JOIN films ON reviews.film_id = films.id GROUP BY film_id ORDER BY rating DESC) AS ranks GROUP BY YEAR(ranks.release_date) ORDER BY YEAR(ranks.release_date) DESC');
    return view('ranks.index')->with('films', $films);
  }
}
