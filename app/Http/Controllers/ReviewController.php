<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Film;
use App\Review;

class ReviewController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth')->except(['show']);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param  Film $film Related movie
     * @return \Illuminate\Http\Response
     */
    public function create(Film $film)
    {
        if ($film->review) {
            session()->flash('error', "Ten film został już zrecenzowany.");
            return redirect()->route('films.show', $film);
        }
        return view('reviews.create')->with('film', $film);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  Film $film
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Film $film)
    {
        $request->validate([
            'rating' => 'required|integer',
            'content' => 'required|string|min:3'
        ]);

        $review = new Review();
        $review->fill(['rating' => $request->rating, 'content' => $request->content]);
        $review->film()->associate($film);

        auth()->user()->reviews()->save($review);
        session()->flash('success', "Dodano recenzję filmu.");

        return redirect()->route('films.show', $film);
    }

    /**
     * Display the specified resource.
     *
     * @param  Review $review
     * @return \Illuminate\Http\Response
     */
    public function show(Review $review)
    {
        return view('reviews.show')->with('review', $review);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Review $review
     * @return \Illuminate\Http\Response
     */
    public function edit(Review $review)
    {
        return view('reviews.edit')->with('review', $review);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  Review $review
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Review $review)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Review $review
     * @return \Illuminate\Http\Response
     */
    public function destroy(Review $review)
    {
        //
    }
}
