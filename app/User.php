<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
  /**
   * System user types.
   */
  const TYPE_User = 1;
  const TYPE_Moderator = 2;
  const TYPE_Administrator = 3;
  const TYPE_Owner = 4;

  use Notifiable;

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
    'nickname', 'email', 'password', 'name', 'surname', 'birthdate', 'type'
  ];

  /**
   * The attributes that should be hidden for arrays.
   *
   * @var array
   */
  protected $hidden = [
    'password', 'remember_token',
  ];

  protected $dates = [
    'created_at', 'updated_at', 'birthdate'
  ];

  public function reviews()
  {
    return $this->hasMany(Review::class);
  }

  public function comments()
  {
    return $this->hasMany(Comment::class);
  }

  public function gravatar($size = 96, $rating = 'g')
  {
    return ('https://www.gravatar.com/avatar/' . md5(strtolower(trim($this->email))) . "?s=$size&r=$rating");
  }

  public function isAdmin()
  {
    return ($this->type == self::TYPE_Owner || $this->type == self::TYPE_Administrator);
  }

  public function isModerator()
  {
    return ($this->type == self::TYPE_Moderator || $this->isAdmin());
  }
}
