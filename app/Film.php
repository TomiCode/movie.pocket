<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Film extends Model
{
  protected $fillable = ['title', 'genre', 'director', 'release_date', 'imgcover', 'category_id'];

  public function reviews()
  {
    return $this->hasMany(Review::class);
  }

  public function review()
  {
    return $this->hasOne(Review::class)->where('user_id', auth()->user()->id);
  }

  public function category()
  {
    return $this->belongsTo(Category::class);
  }
}
