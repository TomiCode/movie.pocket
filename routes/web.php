
<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', 'HomeController@index');
Route::get('/home', 'HomeController@index')->name('home');

Route::resource('films', 'FilmController');
Route::resource('categories', 'CategoryController', [
    'only' => ['index', 'show', 'create', 'store']
]);

Route::prefix('reviews')->group(function() {
  Route::get('create/{film}', 'ReviewController@create')->name('reviews.create');
  Route::post('create/{film}', 'ReviewController@store')->name('reviews.store');
});

Route::resource('reviews', 'ReviewController', [
  'except' => ['index', 'create', 'store']
]);

Route::prefix('comments')->group(function() {
  Route::get('create/{review}', 'CommentsController@create')->name('comments.create');
  Route::post('create/{review}', 'CommentsController@store')->name('comments.store');
  Route::get('delete/{comment}', 'CommentsController@destroy')->name('comments.delete');
});

Route::resource('comments', 'CommentsController', [
  'only' => ['edit', 'update', 'destroy']
]);

Route::prefix('users')->group(function() {
  Route::get('{user}', 'UserController@show')->name('users.show');
  Route::get('edit/{user}', 'UserController@edit')->name('users.edit');
  Route::post('{user}', 'UserController@update')->name('users.update');
});

Route::get('top', 'RankController@index')->name('ranks.index');