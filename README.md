# movie.pocket

System oceny i recenzji filmów online.



## Wymagania

Do uruchomienia projektu wymagane są dodatkowe paczki dla systemu Linux i macOS, które zostały poniżej wyszczególnione.

* PHP (**>= 7.1.3**)
* php-fpm — _tylko dla linucha_
* Nginx
* MariaDB
* Composer (1.6.4)
* laravel-installer (2.0.1)
* laravel-valet (2.0.12) — *tylko dla macOS* 
* Node.js (v9.11.1)
* npm (5.8.0)



## Pliki konfiguracyjne

Poniżej zamieszczone zostały pliki konfiguracyjne, które najeży dostosować do swojego środowiska deweloperskiego.

### Linux

W przypadku pingwinka, projekt powinien znajdować się w katalogu `/usr/share/webapps/movie.pocket`. W innym wypadku należy zmienić ścieżkę w poniższym pliku konfiguracyjnym ustawienia `root`, aby wskazywało na katalog `public` naszego projektu.

Konfiguracja serwera nginx, `/etc/nginx/nginx.conf`:

```nginx
#user html;
worker_processes  1;
error_log  logs/error.log;

events {
  worker_connections  1024;
}

http {
  include       mime.types;
  default_type  application/octet-stream;
  sendfile      on;
  keepalive_timeout  65;
  #gzip  on;

  server {
    listen       80;
    server_name  moviepocket.local;
    root /usr/share/webapps/movie.pocket/public;
    index index.php;
  
    location / {
      try_files $uri $uri/ /index.php?$query_string;
    }
  
    location ~ \.php$ {
      try_files $uri $document_root$fastcgi_script_name =404;
      fastcgi_pass unix:/run/php-fpm/php-fpm.sock;
      fastcgi_index index.php;
      fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
      include fastcgi.conf;
  
      # prevention for httpoxy vulnerability: https://httpoxy.org/
      fastcgi_param HTTP_PROXY "";
    }
  
    #error_page   500 502 503 504  /50x.html;
    #location = /50x.html {
    #    root   /usr/share/nginx/html;
    #}
  }
}
```

Mapowanie adresu lokalnego `moviepocket.local` na serwer nginx przy użyciu pliku `/etc/hosts`:

```Hosts
# Static table lookup for hostnames.
# See hosts(5) for details.
127.0.0.1 moviepocket.local
```

### macOS

Wszystkie pliki konfiguracyjne zawarte są w paczce `laravel-valet`. 



## Przygotowanie środowiska

Do uruchomienia projektu należy pobrać repozytorium git'a, a następnie skonfigurować uprawnienia i plik `.env` naszego projektu, aby umożliwić komunikację z bazą danych.

Sklonowanie repozytorium do katalogu lokalnego
```shell
cd /usr/share/webapps
git clone https://gitlab.com/TomiCode/movie.pocket.git
```

Ustawienie uprawnień i wygenerowanie klucza sesji
```shell
cd movie.pocket
chmod -R 777 storage
cp -p .env.example .env
php artisan key:generate
```

Zmiana konfiguracji bazy danych. Dane powinny być zgodne z informacjami podanymi przy instalacji serwera `mariadb`.

```
DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=moviepocket
DB_USERNAME=root
DB_PASSWORD=112233
```

Po zapisaniu zmian należy zaimportować bazę danych, więc uruchomimy migracje

```Shell
php artisan migrate
```



> Gotowe. Projekt powinien być dostępny pod adresem http://moviepocket.local.